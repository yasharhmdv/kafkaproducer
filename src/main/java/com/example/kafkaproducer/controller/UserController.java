package com.example.kafkaproducer.controller;

import com.example.kafkaproducer.dto.UserDto;
import com.example.kafkaproducer.dto.UserResponseDto;
import com.example.kafkaproducer.dto.UserUpdateBalanceDto;
import com.example.kafkaproducer.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<Void> publish(@RequestBody UserDto userDto) {
        userService.register(userDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/balance/{id}")
    public UserResponseDto addBalance(@PathVariable Long id, @RequestBody UserUpdateBalanceDto balance) {
        return userService.addBalance(id, balance);
    }
}