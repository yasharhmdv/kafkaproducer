package com.example.kafkaproducer.service;

import com.example.kafkaproducer.dto.UserDto;
import com.example.kafkaproducer.dto.UserResponseDto;
import com.example.kafkaproducer.dto.UserUpdateBalanceDto;
import com.example.kafkaproducer.model.User;
import com.example.kafkaproducer.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public void register(UserDto userDto) {
        userRepository.save(modelMapper.map(userDto, User.class));
        kafkaTemplate.send("user-register", userDto);
    }


    public UserResponseDto addBalance(Long id, UserUpdateBalanceDto dto) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        user.setBalance(user.getBalance() + dto.getBalance());
        userRepository.save(user);
        kafkaTemplate.send("user-update-balance", user);

        return modelMapper.map(user, UserResponseDto.class);
    }
}