package com.example.kafkaproducer.repository;

import com.example.kafkaproducer.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
