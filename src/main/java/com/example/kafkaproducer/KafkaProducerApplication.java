package com.example.kafkaproducer;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableKafka
public class KafkaProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaProducerApplication.class, args);
	}

	@RestController
	@RequestMapping("/publish")
	@RequiredArgsConstructor
	public static class ProducerController {

		private final KafkaTemplate<String,String> kafkaTemplate;
		@PostMapping("/{message}")
		public ResponseEntity<Void> publish(@PathVariable String message){
			return  null;
		}

	}
}
